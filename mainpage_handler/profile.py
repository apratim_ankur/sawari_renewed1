from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.contrib import messages
from mainpage_handler.models import UserProfile

def profile(user_name):
    context_profile = {}
    try:
        prof = User.objects.get(username=str(profile_link))
    except:
        raise Http404('Requested user not found.')
    try:
        profile = prof.get_profile()
    except:
        raise Http404("Requested page couldn't be retrieved.")
    if profile:
        loggedin = request.user.is_authenticated()
        not_my_profile = False
        if (request.user != profile.user):
            not_my_profile = True

        request_form = RequestForm()

        if request.method == 'POST':
            type_of_vehicle = request.POST["type_of_vehicle"]
            datetime_to = request.POST["datetime_to"]
            datetime_from = request.POST["datetime_from"]  
            message = request.POST["message"]
            private_request = PrivateRequest.objects.create(sender=request.user.userprofile, receiver=profile, datetime_from=datetime_from,
                datetime_to=datetime_to, message=message)
            messages.info(request, 'Your request has been created')

        sent_requests = PrivateRequest.objects.filter(sender=profile)
        received_requests = PrivateRequest.objects.filter(receiver=profile)
        visitors_requests = None
        if loggedin:
            visitors_requests = PrivateRequest.objects.filter(sender=request.user.get_profile(), receiver=profile)

        context_profile = {'profile': profile, 'request_form': request_form,
                            'not_my_profile': not_my_profile, 'sent_requests': sent_requests,
                            'received_requests': received_requests, 'visitors_requests': visitors_requests }

    return context_profile