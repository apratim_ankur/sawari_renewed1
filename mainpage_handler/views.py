from geopy import geocoders
import json

from django.shortcuts import render_to_response, redirect
from django.template import RequestContext
from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages

from mainpage_handler.models import UserProfile, BaseRequest, OpenRequest, PrivateRequest, Reviews, Trips, Posts

from mainpage_handler.forms import *

from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.cache import cache_page

from mainpage_handler.profile import profile

@csrf_exempt
def mainpage_handler(request):
    msg = ['Welcome to Sawaari' , 'Have an account?', 'New User?']
    query = None
    city=None
    profile_link = None
    lenders = []
    open_request = None
    publicrequests = None
    private_request = None
    review = None
    open_trip = None
    opentrips = None
    context_dir = {}
    post_submit = None
    posts =None
 

    if request.method == 'GET':
        try:
            query = request.GET['query']            
        except:
            pass
        try:
            profile_link = request.GET['profile_link']
        except:
            pass

    if request.method == 'POST':
        try:
            open_request = request.POST['open_request']
        except:
            pass
        try:
            private_request = request.POST['private_request']
        except:
            pass
        try:
            review = request.POST['review']
        except:
            pass
        try:
            open_trip = request.POST['open_trip']
        except:
            pass
        try:
            post_submit = request.POST['post_submit']
        except:
            pass

    if post_submit:
        forum_post = request.POST['forum_post']
        if not query:
            post_location = request.user.userprofile.location
        else:
            post_location = query
        new_post = Posts.objects.create(creator=request.user.userprofile, post = forum_post, location = post_location )
        

    lt, lng = 20.295549, 85.807471
    if query:
        city = ''
        for letter in str(query):
            if letter == ',':
                break
            city = city + str(letter)
            city = str(city)
            # return HttpResponse(query)

        try:
            gmap = geocoders.GoogleV3()
            place, (lt, lng) = gmap.geocode(query)
            # return HttpResponse(lt)
        except:
            # return HttpResponse(lt)
            lt, lng = 20.295549, 85.807471

        lenders = UserProfile.objects.filter(location__icontains=query)
        publicrequests = OpenRequest.objects.filter(location__icontains=query)
        opentrips = Trips.objects.filter(source__icontains=query)
        posts = Posts.objects.filter(location__icontains=query)

    openrequest_location = query

    loggedin = request.user.is_authenticated()
    user = None
    if loggedin:
        user = request.user
        if not query:
            posts = Posts.objects.filter(location__icontains=request.user.userprofile.location)
            posts = posts.order_by('created')

    if open_request:
        openrequest_location = request.GET['query']
        datetime_from = request.POST["datetime_from"]
        datetime_to = request.POST["datetime_to"]
        message = request.POST["message"]
        type_of_vehicle = request.POST["type_of_vehicle"]
        openrequest = OpenRequest.objects.create(sender=request.user.userprofile, datetime_from=datetime_from,
                    datetime_to=datetime_to, message=message, location = openrequest_location)
        context_openrequest =  {'open_request' : open_request}
        context_dir.update(context_openrequest)   
        messages.info(request, 'Your open-request has been created')

    profiles = UserProfile.objects.all()
    usernames = [profile.user.username for profile in profiles if profile.location]
    locations = [profile.location for profile in profiles if profile.location]
    latitudes = []
    longitudes = []
    try:
        gmap = geocoders.GoogleV3()
        for location in locations:
            try:
                lat = gmap.geocode(location)[1][0]
                lang = gmap.geocode(location)[1][1]
                longitudes.append(lang)
                latitudes.append(lat)
            except:
                pass
    except:
        pass

    opentrip_form = OpenTripForm()
        
    context_main = {'head_title': 'Welcome to Sawaari',
                   'page_title': 'Welcome to Sawaari',
                   'loggedin': loggedin,
                   'user': user,
                   'welcome_message' : msg[0],
                   'login_prompt' : msg[1],
                   'signup_prompt' : msg[2] ,
                   'query' : query ,
                   'city' : city ,
                   'lenders' : lenders ,
                   'publicrequests' : publicrequests,
                   'opentrips' :opentrips,
                   'profile_link' : profile_link ,
                   'posts' :posts ,
                   'lt' : lt,
                   'lng': lng,
                   'latitudes': json.dumps(latitudes),
                   'longitudes': json.dumps(longitudes),
                   'usernames': json.dumps(usernames),
                   'opentrip_form' : opentrip_form,
                   }
    context_dir.update(context_main)

    if loggedin:
        user = request.user
        profile = user.userprofile
        context_dir.update({'openrequests' : OpenRequest.objects.filter(location__icontains=profile.location), 'profile':profile})
        if query:
            request_form = RequestForm()
            context_dir.update({'request_form' : request_form})

    if profile_link:
        try:
            prof = User.objects.get(username=str(profile_link))
        except:
            raise Http404('Requested user not found.')
        try:
            profile = prof.get_profile()
        except:
            raise Http404("Requested page couldn't be retrieved.")
        if profile:
            request_form = RequestForm()
            review_form = ReviewForm()
            loggedin = request.user.is_authenticated()
            not_my_profile = False
            if (request.user != profile.user):
                not_my_profile = True

            try:
                gmap = geocoders.GoogleV3()
                place, (lt, lng) = gmap.geocode(profile.location)
            except:
                pass

            reviews = Reviews.objects.filter(receiver=(User.objects.get(username=request.GET['profile_link']).get_profile()))

            sent_requests = PrivateRequest.objects.filter(sender=profile)
            received_requests = PrivateRequest.objects.filter(receiver=profile)
            visitors_requests = None
            if loggedin:
                visitors_requests = PrivateRequest.objects.filter(sender=request.user.get_profile(), receiver=profile)

            context_profile = {'profile': profile, 'request_form': request_form,
                                'not_my_profile': not_my_profile, 'sent_requests': sent_requests,
                                'received_requests': received_requests, 'visitors_requests': visitors_requests,'lt':lt,
                                'lng':lng ,'profile_location':profile.location, 'review_form': review_form, 'reviews' : reviews}
            context_dir.update(context_profile)

    if review:
        new_review = Reviews.objects.create(sender=request.user.userprofile, receiver=(User.objects.get(username=request.GET['profile_link']).get_profile()),
            experience=request.POST['experience'], review=request.POST['review_message'])
        messages.info(request, 'Your Review Has Been Posted')


    if private_request:
        
        type_of_vehicle = request.POST["type_of_vehicle"]
        datetime_to = request.POST["datetime_to"]
        datetime_from = request.POST["datetime_from"]  
        message = request.POST["message"]
        private_request = PrivateRequest.objects.create(sender=request.user.userprofile,
                                                        receiver=(User.objects.get(username=request.GET['profile_link']).get_profile()),
                                                        datetime_from=datetime_from,
                                                        datetime_to=datetime_to, message=message)
        messages.info(request, 'Your request has been created')


    if open_trip:
        source = request.POST['source']
        destination = request.POST['destination']
        new_trip = Trips.objects.create(creator=request.user.userprofile, source=source,destination=destination)
        messages.info = (request, 'Your trip has been created')        


    return render_to_response('mainpage_handler.html', context_dir, context_instance=RequestContext(request))
    

@csrf_exempt
def signup(request):
    if request.method == 'POST':
        username = request.POST['username']
        email = request.POST['email']
        first_name = request.POST['first_name']
        last_name = request.POST['last_name']
        location = request.POST['location']
        try:
            gmap = geocoders.GoogleV3()
            place, (lt, lng) = gmap.geocode(location)
        except:
            pass
        password = request.POST['password']
        verify_password = request.POST['verify_password']
        if password == verify_password:
            user = User.objects.create_user(username=username, email=email, password=password)
            user.first_name = first_name
            user.last_name = last_name
            user.save()
            custom_user = user.get_profile()
            custom_user.location = location
            custom_user.save()
            user = authenticate(username=username, password=password)
            login(request, user)
            return HttpResponseRedirect('/?profile_link=%s'%(username))
    form = UserForm()
    context_dir = {'form': form}
    return render_to_response('signup.html', context_dir)

@csrf_exempt
def login_handler(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                return redirect('/?profile_link=%s'%(username))
            else:
                return HttpResponse("disabled account")
        else:
            return HttpResponse("Invalid Login")
    if request.user.is_authenticated():
        return redirect('/profile/%s'%(request.user.username))
    return render_to_response('login.html', {'form': LoginForm() })


def logout_handler(request):
    logout(request)
    return redirect('/')
