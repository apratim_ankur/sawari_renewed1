    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script>
    <script type="text/javascript">
      function initialize() {
        var mapOptions = {
          center: new google.maps.LatLng({{lt}},{{lng}}),
          {%if query%}
          zoom: 8,
          {%else%}
          zoom: 2,
          {%endif%}
          mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var mapdiv = document.getElementById("map_canvas");
        var map = new google.maps.Map(mapdiv, mapOptions);

        var input = document.getElementById('place-search');
        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.bindTo('bounds', map);

        latitudes = {{latitudes}};
        longitudes = {{longitudes}};
        usernames = {{usernames|safe}};

        console.log(latitudes);
        var markers = [];
        var infowindows = [];
        for (var i = 0; i < latitudes.length; i++) {
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(latitudes[i], longitudes[i]),
                title: usernames[i]
            });
            var contentString = '<a href="/?profile_link=' + usernames[i] +'">' + usernames[i] +'\'s profile';

            var infowindow = new google.maps.InfoWindow({
                content: contentString
            });
            markers.push(marker);
            infowindows.push(infowindow);
        };
        console.log(infowindows[2]);

        function infowindowtrigger (infowindows, markers, i) {
            return function(){
                infowindows[i].open(map, markers[i]);
            };
        };

        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(map);
            google.maps.event.addListener(markers[i], 'click', infowindowtrigger(infowindows, markers, i));
        };
        
      }
    </script>