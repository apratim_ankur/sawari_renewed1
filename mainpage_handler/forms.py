from django import forms
from django.forms import ModelForm
from django.contrib.auth.models import User

from mainpage_handler.models import UserProfile, BaseRequest, Trips

class UserForm(ModelForm):
    class Meta:
        model = User
        fields = ('username', 'email', 'first_name', 'last_name', 'password')
    verify_password = forms.CharField(widget=forms.PasswordInput())
    location = forms.CharField()



class RequestForm(forms.Form):
    available_types = (
        ('SUV', 'SUV'),
        ('SEDAN', 'Sedan'),
        ('MotorBike', 'MotorBIKE'),
        ('Cycle', 'CYCLE'),
        ('any', 'any'),
    )
    datetime_to = forms.DateTimeField(widget=forms.DateTimeInput(attrs={'class': 'datetime'}))
    datetime_from = forms.DateTimeField(widget=forms.DateTimeInput(attrs={'class': 'datetime'}))
    type_of_vehicle = forms.ChoiceField(choices=available_types)
    message = forms.CharField()


class LoginForm(forms.Form):
	username = forms.CharField(max_length=30)
	password = forms.CharField(widget=forms.PasswordInput())

class ReviewForm(forms.Form):
    experience_types = ( 
                        ('Positive' , 'positive'),
                        ('Neutral' , 'neutral'),
                        ('Negative' , 'negative')
                        )
    experience = forms.ChoiceField(choices=experience_types)
    review_message = forms.CharField(max_length=300)

class OpenTripForm(forms.Form):
    class Meta:
        model=Trips
        fields=('source','destination','via')
    source = forms.CharField(max_length=200)
    destination = forms.CharField(max_length=200)
    datetime = forms.DateTimeField(widget=forms.DateTimeInput(attrs={'class': 'datetime'}))