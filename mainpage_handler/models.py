from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save

from datetime import date, timedelta

from geopy import geocoders

class UserProfile(models.Model):
    user = models.OneToOneField(User)
    location = models.CharField(max_length=30)
    rating = models.IntegerField(blank=True, null=True)
    contactno = models.IntegerField(blank=True, null=True)
    vouch_level = models.IntegerField(blank=True, null=True)
    pics = models.FileField(upload_to='E:\INFO\APP-OX\programming\python\projects\sawari_renewed1\sawari_renewed1\media', null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
	
    def __unicode__(self):
	    return self.user.first_name

def user_post_save(sender, instance, created, **kwargs):
    """Create a user profile when a new user account is created"""
    if created:
        UserProfile.objects.create(user=instance)

post_save.connect(user_post_save, sender=User)

class Vehicle(models.Model):
    vehicle_types = (
        ('SUV', 'SUV'),
        ('SEDAN', 'Sedan'),
        ('MotorBike', 'MotorBIKE'),
        ('Cycle', 'CYCLE'),
        ('Other', 'other'),
        )
    available_vehicle_types = models.CharField(max_length=10, choices=vehicle_types)
    created = models.DateTimeField(auto_now_add=True)
    pics = models.FileField(upload_to='E:\INFO\APP-OX\programming\python\projects\sawari_renewed1\sawari_renewed1\media')
    owner = models.OneToOneField(UserProfile, null=True, blank=True)

class BaseRequest(models.Model):
    sender = models.ForeignKey('UserProfile', related_name='sender')   
    message = models.CharField(max_length=50)
    datetime_from = models.DateTimeField()
    datetime_to = models.DateTimeField()
    status = models.NullBooleanField()
    comments = models.CharField(max_length=100,blank=True, null=True)
    vehicle_type = models.CharField(max_length=120)
    created = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.message

class OpenRequest(BaseRequest):
    location = models.CharField(max_length=100)
    sharer = models.ManyToManyField(UserProfile,blank=True, null=True, related_name='openreq_sharer')
    bidder = models.ManyToManyField(UserProfile,blank=True, null=True, related_name='openreq_bidder' )

class PrivateRequest(BaseRequest):
    receiver = models.ForeignKey(UserProfile)


class Reviews(models.Model):
    sender = models.ForeignKey(UserProfile, related_name='reviewer')
    receiver = models.ForeignKey(UserProfile)
    experience_types = ( 
                        ('Positive' , 'positive'),
                        ('Neutral' , 'neutral'),
                        ('Negative' , 'negative')
                        )
    experience = models.CharField(max_length=10, choices=experience_types)
    review = models.CharField(max_length=300)
    created = models.DateTimeField(auto_now_add=True)

class Trips(models.Model):
    trip_id = models.IntegerField(unique=True)
    creator = models.ForeignKey(UserProfile)
    source = models.CharField(max_length=100)
    destination = models.CharField(max_length=100)
    via = models.CharField(max_length=300,null=True,blank=True)
    sharer = models.ManyToManyField(UserProfile,null=True,blank=True, related_name='trip_sharer')
    created = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.destination

class Posts(models.Model):
    post = models.CharField(max_length=500)
    creator = models.ForeignKey(UserProfile)
    created = models.DateTimeField(auto_now_add=True)
    location = models.CharField(max_length=200)

    def __unicode__(self):
        return self.post

class Comments(models.Model):
    post = models.ForeignKey(Posts)
    comments = models.CharField(max_length=500)
    created = models.DateTimeField(auto_now_add=True)

class vouch(models.Model):
    voucher = models.ForeignKey(UserProfile)
    vouched = models.ForeignKey(UserProfile, related_name='vouched_user')