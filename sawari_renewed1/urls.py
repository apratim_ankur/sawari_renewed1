from django.conf.urls import patterns, include, url
import mainpage_handler.views
from django.conf import settings


from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
    url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT}),
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),
	
	
    url(r'^$', mainpage_handler.views.mainpage_handler),
	url(r'^login/$', mainpage_handler.views.login_handler),
    url(r'^signup/$', mainpage_handler.views.signup), 	
    url(r'^logout/$', mainpage_handler.views.logout_handler),

)
